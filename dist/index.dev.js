"use strict";

var API_URL = "https://www.cloudflare.com/cdn-cgi/trace";
var IP;

function onDataRecieve() {
  IP = xhttp.responseText.split('\n')[2].split('=')[1];

  if (IP == '2601:283:8100:1a61:1996:7d00:5eca:1905') {
    document.querySelector('[href="https://mail.google.com"]').href = 'https://mail.google.com/mail/u/2/';
  } else {}
}

var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = onDataRecieve;
xhttp.open("GET", API_URL, true);
xhttp.send();
var aimsInterval;

function loadUI() {
  if (!window.location.hash.includes('frcc')) {
    var s = document.createElement('style');
    s.id = 'frccHide';
    s.innerHTML = "\n        .fr a, .fr div {\n    display: none;\n}";
    document.head.appendChild(s);
  }

  if (!window.location.hash.includes('tcc')) {
    var st = document.createElement('style');
    st.id = 'tccHide';
    st.innerHTML = "\n        .tcc >  a, .tcc > div {\n    display: none;\n        }\n    .addTCC  {\n        display:flex !important    ;\n    }\n";
    document.head.appendChild(st);
  } else {
    setInterval(function () {
      if (document.querySelector('.addTCC')) {
        document.querySelector('.addTCC').remove();
      }
    }, 100);
  }

  if (!window.location.hash.includes('frcc')) {
    var _st = document.createElement('style');

    _st.id = 'frccHide';
    _st.innerHTML = "\n        .fr >  a, .fr > div {\n    display: none;\n        }\n    .addFRCC  {\n        display:flex !important    ;\n    }\n    .addFRCC div {\n        display: block;\n    }\n";
    document.head.appendChild(_st);
  } else {
    setInterval(function () {
      if (document.querySelector('.addFRCC')) {
        document.querySelector('.addFRCC').remove();
      }
    }, 100);
  }
}

loadUI();

function addTCC() {
  location.hash = location.hash + 'tcc';
  $('#tccHide').remove();
  loadUI();
}

function addFRCC() {
  location.hash = location.hash + 'frcc';
  $('#frccHide').remove();
  loadUI();
  window.location.reload();
}